from flask import Flask

app = Flask(__name__)


@app.route('/math-parser/')
def math_parser_index():
    return 'Task 1 - Math parser'


@app.route('/math-parser/<input_string>')
def math_parser(input_string):
    import re
    import operator

    translate_operations = {"plus": operator.add, "minus": operator.sub}

    # check if number is the first character
    if re.match(r'^\D+', input_string):
        return 'Number must be first in the string'

    # check if number is the last character
    if re.match(r'.*\D+$', input_string):
        return 'Number must be last in the string'

    # validate approved operations
    valid_operations = ['plus', 'minus']
    operators = re.findall(r'\D+', input_string)

    for op in operators:
        if op not in valid_operations:
            return 'Invalid operation(s) specified'

    # get list of all numbers
    numbers = re.findall(r'\d+', input_string)

    # do the math
    for x in range(len(operators)):
        math = operators[x]

        if x == 0:
            output = translate_operations[math](int(numbers[x]), int(numbers[x + 1]))
        else:
            output = translate_operations[math](int(output), int(numbers[x + 1]))

    return str(output)


@app.route('/blackjack/')
def blackjack_scorer_index():
    return 'Task 2 - Blackjack scorer'


@app.route('/blackjack/<input_string>')
def blackjack_scorer(input_string):
    import re

    blackjack = 21
    convert_ace = False
    cards = input_string.split(',')

    hand_value = 0
    aces_to_convert = 0
    cards_count = len(cards)
    for x in cards:
        if re.match(r'^\d+', x):
            if int(x) < 2 or int(x) > 10:
                return 'Allowed cards are 2,3,4,5,6,7,8,9,10,J,Q,K,A'
            else:
                hand_value += int(x)
        elif re.match(r'^\D+', x):
            # check if the card is ace
            if x == 'A':
                convert_ace = True
                aces_to_convert += 1
            elif x.upper() == 'J' or x.upper() == 'Q' or x.upper() == 'K':
                hand_value += 10
            else:
                return 'Unknown card in your hand. Are you cheating?'
        else:
            return 'Unknown data type'

    # check if we have ace in our hand
    if convert_ace:
        for x in range(aces_to_convert):
            # how many aces do we have in our hand?
            if aces_to_convert > 1:
                # do we have other cards in our hand as well?
                if cards_count > 2:
                    # we have other cards as well, can´t exceed 21
                    hand_value += 1
                else:
                    # looks like we have 22!
                    hand_value += 11
            else:
                if hand_value <= 10:
                    # we can count ace for 11 points without exceeding 21
                    hand_value += 11
                else:
                    # watch out, 21 is near
                    hand_value += 1

    return str(hand_value)


@app.route('/blending-arrays/')
def blending_arrays_index():
    return '[]'


@app.route('/blending-arrays/<input_string>')
def blending_arrays(input_string):
    # first split string into arrays on ';'
    arrays = input_string.split(';')

    # if only one array is present, return it back as a response
    if len(arrays) == 1:
        return str(input_string)
    # more arrays in the input
    else:
        # get array with highest index
        highest_index = 0
        for x in range(len(arrays)):
            sub_array = arrays[x].split(',')
            index = len(sub_array)
            if index > highest_index:
                highest_index = index

        # now merge arrays into one
        new_array = []
        for z in range(highest_index):
            for y in range(len(arrays)):
                sub_array = arrays[y].split(',')
                if z >= len(sub_array):
                    new_value = 'None'
                else:
                    new_value = sub_array[z]
                new_array.append(new_value)

    return str(new_array)
    # for array in arrays:


if __name__ == '__main__':
    app.run(debug=True)
