# Python developer - interview task

## Part One

### Task one - Math parser
The goal of this task was to do simple mathematical operations written in a string.
To solve this I have used regular expressions to get to separate lists of numbers and operations.
I have also took the advantage of python **operator** module to help me convert the description of the operation
into actual mathematical operation. There are three very simle validations performed:

* input string must start with a digit
* input string must end with a digit
* only two operations are allowed (plus,minus)

To test this task, run CSOB.py file. It will automatically start a simple HTTP server on your local machine.
The go to **http://127.0.0.1:5000/math-parser/1plus1plus15minus5**, where **1plus1plus15minus5** is the input
string. The result will be return in a form of HTTP response and will be shown in the browser.

### Task two - Blackjack scorer
Return total value of cards in hand. If there are aces in hand, decide whether they should count as 11 or 1.
To test this task go to **http://127.0.0.1:5000/blackjack/5,4,3,2,A,K**, where **5,4,3,2,A,K** are the cards
in hand. The cards have to be separated by a comma **,**. The is a simple validation to check card has either value
2 to 10 or J,Q,K or A for Jack, Queen, King and Ace. Result will be written in a form of HTTP response again.
There is a small algorithm to decide, what value should Ace have. The reason why it was used the way it was
can be explained later on during a meeting.

### Task three - Blending arrays
Merge provided arrays into one. Arrays are provided as follows: **http://127.0.0.1:5000/blending-arrays/a,b,c;1,2,3,4**.
Items of an array are separated by a comma - ** ,** - , arrays are separated using a semicolon - **;** - .

## Part Two
This part focuses on the Flask framework and it was meant to take tasks from Part One and wrap them up in Flask.